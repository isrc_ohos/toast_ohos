# Toast_ohos

##### 项目介绍

- 项目名称：Toast轻提示
- 所属系列：鸿蒙的第三方组件适配移植
- 编程语言：JavaScript
- 功能：本项目包含2种轻提示框的封装
- 开发版本：sdk7，DevEco Studio 3.0 Beta2
- 项目发起作者：曹天恒
- 邮箱：[isrc_hm@iscas.ac.cn](mailto:isrc_hm@iscas.ac.cn)

![](readme_image/1.gif)

##### 使用教程

1. 组件的实现位于 entry/src/main/js/default/common/components/ 中，将组件复制至自己的项目中即可调用这些组件。

2. 在 hml 文件中添加Toast控件。

   ```html
   <!-- index.hml -->
   <!-- 指定自定义组件的命名与位置-->
   <element name="toast" src="../../common/components/toast/toast.hml">
   </element>
   
   <!--在页面中引入组件-->
   <toast id="myToast"></toast>
   ```
   
3. 在自己的项目中调用显示Toast函数提示框：
    调用代码：
    
   ```js
   //index.js
   myFunction1() {
     this.$child('myToast').show_toast('正常', 0, 1000)
   }
   ```
   

    参数：

   | 参数名     | 解释                                                         |
   | ---------- | ------------------------------------------------------------ |
   | text       | 文字内容                                                     |
   | toast_type | 提示框类型（0、1、2为底部，3、4、5为带图标居中，颜色分别为灰、绿、红） |
   | time       | 持续时间（包含动画时间，单位：毫秒）                         |


##### 版本迭代

- v0.1.0-alpha

##### 版权和许可信息

- Toast_ohos经过[Apache License, version 2.0](https://gitee.com/link?target=http%3A%2F%2Fwww.apache.org%2Flicenses%2FLICENSE-2.0)授权许可。

